$(function() {
    //slide owl-carousel
    $(".owl-carousel").owlCarousel({
        items: 2,
        autoplay: true,
        autoplaySpeed: 300,
        autoplayTimeout: 2500,
        margin: 15,
        loop: true
    });

    //fixed header
    $(window).scroll(function() {
        if ($(this).scrollTop() > 1) {
            $('.nav').addClass('sticky');
        }
        else {
            $('.nav').removeClass('sticky');
        }
    });



    //back to top button
    const btn = $('#backToTop');

    $(window).on('scroll', function() {
        if ($(window).scrollTop() > 520) {
            btn.addClass('show');
        } else {
            btn.removeClass('show');
        }
    });

    btn.on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({scrollTop:0}, '300');
    });

    //loading
    $(window).on('load', function() {
        setInterval(function () {
            $("#loading").fadeOut("slow");
        }, 3000);
    });

    $("header").ripples();


    var scene = document.getElementById('scene');
    var parallaxInstance = new Parallax(scene, {
        relativeInput: true
    });

});